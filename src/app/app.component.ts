import { Component, OnInit } from '@angular/core';
import Map from "@arcgis/core/Map";
import SceneView from "@arcgis/core/views/SceneView";
import SceneLayer from "@arcgis/core/layers/SceneLayer";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'jsapi-scene-issue';

  ngOnInit(): void {
    const sceneLayer = new SceneLayer({
      url: 'https://services3.arcgis.com/yLK8EZgmDk1036UW/arcgis/rest/services/empirical_buildings/SceneServer/0'
      // url: 'https://services3.arcgis.com/yLK8EZgmDk1036UW/arcgis/rest/services/extruded_buildings/SceneServer/0'
    });

    const map = new Map({
      basemap: 'osm',
      layers: [sceneLayer]
    });

    const sceneView = new SceneView({
      map: map,
      camera: {
        position: {
          x: 151.215511,
          y: -33.855924,
          z: 470
        },
        heading: 217,
        tilt: 70
      },
      container: "map"
    });
  }
}
