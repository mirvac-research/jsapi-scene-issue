# JsapiSceneIssue

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 12.2.9.

## Development server

Run `npm run dev-run` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Production server

Run `npm run test-run` for a prod server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.
